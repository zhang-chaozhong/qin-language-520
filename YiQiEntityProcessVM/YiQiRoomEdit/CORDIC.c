#include <stdio.h>  
#include <math.h>  
  
#define N 16  // 迭代次数，可以根据需要调整  
  
// CORDIC常数  
const double K = 0.60725293510239146548;  // 增益因子  
const double angles[N] = {  
    atan(1.0 / pow(2, 0)),  
    atan(1.0 / pow(2, 1)),  
    atan(1.0 / pow(2, 2)),  
    atan(1.0 / pow(2, 3)),  
    atan(1.0 / pow(2, 4)),  
    atan(1.0 / pow(2, 5)),  
    atan(1.0 / pow(2, 6)),  
    atan(1.0 / pow(2, 7)),  
    atan(1.0 / pow(2, 8)),  
    atan(1.0 / pow(2, 9)),  
    atan(1.0 / pow(2, 10)),  
    atan(1.0 / pow(2, 11)),  
    atan(1.0 / pow(2, 12)),  
    atan(1.0 / pow(2, 13)),  
    atan(1.0 / pow(2, 14)),  
    atan(1.0 / pow(2, 15))  
};  
  
// 旋转方向（0为逆时针，-1为顺时针）  
int direction[N];  
  
// CORDIC算法实现  
void cordic(double theta, double *sin_theta, double *cos_theta) {  
    double x = 1.0, y = 0.0;  // 初始向量  
    double d, sigma = theta;  
  
    // 初始化方向数组  
    for (int i = 0; i < N; i++) {  
        if (sigma >= 0) {  
            direction[i] = 0;  
            sigma -= angles[i];  
        } else {  
            direction[i] = -1;  
            sigma += angles[i];  
        }  
  
        d = direction[i] * pow(2, -i);  
        double new_x = x - d * y;  
        y = y + d * x;  
        x = new_x;  
    }  
  
    // 应用增益因子  
    *sin_theta = K * y;  
    *cos_theta = K * x;  
}  
  
int main() {  
    double theta, sin_theta, cos_theta;  
  
    // 示例：计算45度（π/4弧度）的正弦和余弦  
    theta = M_PI / 4;  
    cordic(theta, &sin_theta, &cos_theta);  
  
    printf("sin(%f) = %f\n", theta, sin_theta);  
    printf("cos(%f) = %f\n", theta, cos_theta);  
  
    return 0;  
}