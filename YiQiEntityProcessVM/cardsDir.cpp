#include <iostream>  // 包含输入输出流库
#include <string>    // 包含字符串处理库
#include <vector>    // 包含动态数组库
#include <memory>    // 包含智能指针库

// 定义一个抽象基类DirectoryItem，表示目录项
class DirectoryItem {
public:
    virtual ~DirectoryItem() {}  // 虚析构函数确保派生类可以被正确地销毁
    virtual void displayInfo() const = 0;  // 纯虚函数，定义显示信息的行为
};

// Directory类继承自DirectoryItem，表示一个目录
class Directory : public DirectoryItem {
    std::vector<std::unique_ptr<DirectoryItem>> items;  // 存储目录中的子项
public:
    // 添加一个子项到目录中
    void addItem(std::unique_ptr<DirectoryItem> item) {
        items.push_back(std::move(item));  // 使用移动语义插入新元素
    }

    // 显示目录的内容
    void displayInfo() const override {
        std::cout << "Directory contents:\n";  // 输出目录标题
        for (const auto& item : items) {  // 遍历所有子项
            item->displayInfo();  // 调用子项的displayInfo方法
        }
    }
};

// CardFile类继承自DirectoryItem，表示一个具体的文件（这里是指卡牌）
class CardFile : public DirectoryItem {
    std::string name;  // 卡牌的名字
public:
    // 构造函数
    CardFile(std::string name) : name(name) {}

    // 显示卡牌的信息
    void displayInfo() const override {
        std::cout << "Card: " << name << "\n";  // 输出卡牌名称
    }
};

int main() {
    // 创建根目录
    std::unique_ptr<Directory> root = std::make_unique<Directory>();
    // 创建Cards目录
    std::unique_ptr<Directory> cardsDir = std::make_unique<Directory>();

    // 向Cards目录中添加卡牌
    cardsDir->addItem(std::make_unique<CardFile>("Arsenalship"));
    cardsDir->addItem(std::make_unique<CardFile>("Explorationship"));

    // 将Cards目录添加到根目录
    root->addItem(std::move(cardsDir));

    // 打印根目录的内容
    root->displayInfo();

    return 0;
}