# 导入argparse模块用于处理命令行参数  
import argparse  
# 导入pprint模块用于美化打印  
import pprint  
  
# 导入rlcard库，这是一个用于构建和研究AI在扑克牌类游戏中的框架  
import rlcard  
# 从rlcard.agents导入RandomAgent，这是一个简单的随机选择动作的代理  
from rlcard.agents import RandomAgent  
# 从rlcard.utils导入set_seed函数，用于设置随机数种子以保证结果的可重复性  
from rlcard.utils import set_seed  
  
# 定义run函数，它接受命令行参数作为输入  
def run(args):  
    # 使用rlcard.make函数根据传入的环境名称创建环境实例  
    # 这里还通过config参数设置了随机数种子为42  
    env = rlcard.make(  
        args.env,  
        config={  
            'seed': 42,  
        }  
    )  
  
    # 使用set_seed函数设置numpy, torch, random的随机数种子，以确保实验的可重复性  
    set_seed(42)  
  
    # 创建一个RandomAgent实例，num_actions参数设置为环境中可能的动作数量  
    agent = RandomAgent(num_actions=env.num_actions)  
    # 为环境中的每个玩家设置一个RandomAgent代理  
    env.set_agents([agent for _ in range(env.num_players)])  
  
    # 在非训练模式下运行环境，生成轨迹（trajectories）和玩家胜负记录（player_wins）  
    trajectories, player_wins = env.run(is_training=False)  
    # 打印出所有生成的轨迹  
    print('\nTrajectories:')  
    print(trajectories)  
    # 打印出第一条轨迹的第一个状态中的原始观测值  
    print('\nSample raw observation:')  
    pprint.pprint(trajectories[0][0]['raw_obs'])  
    # 打印出第一条轨迹的第一个状态中的合法动作列表  
    print('\nSample raw legal_actions:')  
    pprint.pprint(trajectories[0][0]['raw_legal_actions'])  
  
# 如果直接运行这个脚本，则执行以下代码  
if __name__ == '__main__':  
    # 创建ArgumentParser对象，用于解析命令行参数  
    parser = argparse.ArgumentParser("Random example in RLCard")  
    # 添加一个命令行参数'--env'，类型为字符串，默认值为'leduc-holdem'  
    # 并设置可选值列表，表示支持的环境名称  
    parser.add_argument(  
        '--env',  
        type=str,  
        default='leduc-holdem',  
        choices=[  
            'blackjack',  
            'leduc-holdem',  
            'limit-holdem',  
            'doudizhu',  
            'mahjong',  
            'no-limit-holdem',  
            'uno',  
            'gin-rummy',  
            'bridge',  
        ],  
    )  
  
    # 解析命令行参数  
    args = parser.parse_args()  
  
    # 调用run函数，传入解析后的命令行参数  
    run(args)