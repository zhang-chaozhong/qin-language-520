#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

class PhysicalCharacteristic {
public:
    std::string description;
    PhysicalCharacteristic(const std::string& desc) : description(desc) {}
};

class GeographicInfo {
public:
    std::string location;
    GeographicInfo(const std::string& loc) : location(loc) {}
};

class YiQiEntity {
public:
    std::unique_ptr<PhysicalCharacteristic> physicalChar;
    std::unique_ptr<GeographicInfo> geoInfo;

    YiQiEntity(const std::string& physicalDesc, const std::string& location)
        : physicalChar(std::make_unique<PhysicalCharacteristic>(physicalDesc)),
          geoInfo(std::make_unique<GeographicInfo>(location)) {}

    virtual ~YiQiEntity() = default;
};

class YiQiEntityGeoInfo : public YiQiEntity {
public:
    std::shared_ptr<GeographicInfo> sharedGeoInfo;

    YiQiEntityGeoInfo(const std::string& physicalDesc, const std::string& location)
        : YiQiEntity(physicalDesc, location),
          sharedGeoInfo(std::make_shared<GeographicInfo>(location)) {}
};

class YiQiEntityAlias {
public:
    std::unique_ptr<YiQiEntity> person;
    std::unique_ptr<YiQiEntity> organization;
    std::vector<std::unique_ptr<YiQiEntityGeoInfo>> geoInfos;

    YiQiEntityAlias(std::unique_ptr<YiQiEntity> p, std::unique_ptr<YiQiEntity> o)
        : person(std::move(p)), organization(std::move(o)) {}

    void addGeoInfo(std::unique_ptr<YiQiEntityGeoInfo> info) {
        geoInfos.push_back(std::move(info));
    }
};

PYBIND11_MODULE(yiqi_entity, m) {
    py::class_<PhysicalCharacteristic>(m, "PhysicalCharacteristic")
        .def(py::init<const std::string&>())
        .def_readwrite("description", &PhysicalCharacteristic::description);

    py::class_<GeographicInfo>(m, "GeographicInfo")
        .def(py::init<const std::string&>())
        .def_readwrite("location", &GeographicInfo::location);

    py::class_<YiQiEntity>(m, "YiQiEntity")
        .def(py::init<const std::string&, const std::string&>())
        .def("get_physical_characteristic", &YiQiEntity::physicalChar->description)
        .def("get_geographic_info", &YiQiEntity::geoInfo->location);

    py::class_<YiQiEntityGeoInfo, YiQiEntity>(m, "YiQiEntityGeoInfo")
        .def(py::init<const std::string&, const std::string&>())
        .def("get_shared_geographic_info", &YiQiEntityGeoInfo::sharedGeoInfo->location);

    py::class_<YiQiEntityAlias>(m, "YiQiEntityAlias")
        .def(py::init<std::unique_ptr<YiQiEntity>, std::unique_ptr<YiQiEntity>>())
        .def("add_geo_info", &YiQiEntityAlias::addGeoInfo)
        .def("get_person", &YiQiEntityAlias::person->physicalChar->description)
        .def("get_organization", &YiQiEntityAlias::organization->physicalChar->description);
}