这段代码定义了一个简单的消息传递系统和实体管理框架。下面是对每一部分的详细注释解释：

### 基础类定义

#### `Message` 类
- **作用**：表示在实体之间发送的消息。
- **成员变量**：
  - `content`: 消息的内容。
  - `recipientId`: 接收者的ID。
- **构造函数**：接受一个字符串和一个接收者ID作为参数，并将它们赋值给成员变量。

#### `MessageQueue` 类
- **作用**：管理消息队列。
- **成员变量**：
  - `messages`: 存储消息的向量。
- **成员函数**：
  - `enqueue`: 将消息添加到队列末尾。
  - `isEmpty`: 检查队列是否为空。
  - `dequeue`: 从队列头部移除并返回一条消息，如果队列为空则抛出异常。

#### `MessageSubscriber` 类
- **继承自**：`MessageSubscriber` 继承自 `MessageQueue`（实际代码中并未直接继承，这里是指它也使用了 `MessageQueue` 类）。
- **作用**：表示能够接收消息的实体。
- **成员变量**：
  - `id`: 实体的唯一标识符。
  - `queue`: 接收到的消息队列。
- **构造函数**：接受一个ID作为参数。
- **成员函数**：
  - `receiveMessage`: 根据接收者ID接收消息。
  - `processMessages`: 处理接收到的消息队列中的所有消息。

### 具体实体类

#### `AtomComposition` 类
- **作用**：表示特定类型的原子组合。
- **成员变量**：
  - `type`: 原子组合的类型。
- **构造函数**：接受一个字符串类型参数。

#### `LeadReadingPlan` 枚举类
- **作用**：表示不同的计划或状态。

#### `YiQiEntity` 类
- **作用**：表示一个通用实体。
- **成员变量**：
  - `name`: 实体的名称。
- **构造函数**：接受一个字符串名称参数。
- **成员函数**：
  - `printInfo`: 打印实体的信息。
  - `storeData`: 存储数据。
  - `communicate`: 发送消息。
  - `compute`: 执行计算操作。

#### `YiQiEntityVM` 类
- **作用**：表示一个虚拟机实体。
- **成员变量**：
  - `entity`: 关联的 `YiQiEntity` 对象。
  - `plan`: 关联的 `LeadReadingPlan` 计划。
- **构造函数**：接受一个 `YiQiEntity` 对象指针和一个 `LeadReadingPlan` 参数。
- **成员函数**：
  - `printInfo`: 打印关联实体的信息。
  - `storeData`: 存储数据。
  - `communicate`: 发送消息。
  - `compute`: 执行计算操作。

#### `YiQiEntityVMAtomComposition` 类
- **作用**：结合了 `AtomComposition` 和 `YiQiEntityVM` 功能性的实体。
- **成员变量**：
  - `composition`: 原子组合。
  - `vm`: 虚拟机实体。
- **构造函数**：接受一个ID、一个原子组合类型字符串、一个 `YiQiEntity` 对象指针和一个 `LeadReadingPlan` 参数。
- **成员函数**：
  - `printVMInfo`: 打印虚拟机信息。
  - `storeData`: 存储数据。
  - `communicate`: 发送消息。
  - `compute`: 执行计算操作。

### 实体管理类

#### `YiQiEntityVMManager` 类
- **作用**：管理多个 `YiQiEntityVMAtomComposition` 实例。
- **成员变量**：
  - `subscribers`: 存储订阅者的映射表。
  - `globalQueue`: 全局消息队列。
- **成员函数**：
  - `addSubscriber`: 添加订阅者。
  - `removeSubscriber`: 移除订阅者。
  - `getSubscriber`: 获取指定ID的订阅者。
  - `sendMessage`: 向全局消息队列发送消息。
  - `processGlobalMessages`: 处理全局消息队列中的消息。
  - `globalCompute`: 执行所有订阅者的计算操作。
  - `globalStoreData`: 向所有订阅者存储数据。
  - `globalCommunicate`: 向所有订阅者发送消息。

### 示例用法

在 `main` 函数中，创建了一个 `YiQiEntity` 实例、一个 `YiQiEntityVMAtomComposition` 实例和一个 `YiQiEntityVMManager` 实例。然后向全局消息队列发送了一条消息，并处理了这些消息。最后执行了一些全局操作，如计算、存储数据和通信。


```cpp
#include <iostream>          // For basic input/output streams
#include <string>            // For string handling
#include <memory>            // For smart pointers
#include <vector>            // For dynamic arrays
#include <unordered_map>     // For hash map

// Message class represents a message sent between entities.
class Message {
public:
    std::string content;     // The content of the message.
    size_t recipientId;      // The ID of the recipient.

    // Constructor initializes the message with content and recipient ID.
    Message(std::string content, size_t recipientId)
        : content(std::move(content)), recipientId(recipientId) {}
};

// MessageQueue class manages a queue of messages.
class MessageQueue {
public:
    std::vector<Message> messages;  // The queue of messages.

    // Enqueues a message to the queue.
    void enqueue(const Message& msg) {
        messages.push_back(msg);
    }

    // Checks if the queue is empty.
    bool isEmpty() const {
        return messages.empty();
    }

    // Dequeues a message from the front of the queue.
    Message dequeue() {
        if (!messages.empty()) {
            Message front = messages.front();
            messages.erase(messages.begin());
            return front;
        }
        throw std::runtime_error("Queue is empty.");
    }
};

// MessageSubscriber class represents an entity that can receive messages.
class MessageSubscriber {
public:
    size_t id;                // The ID of the subscriber.
    MessageQueue queue;       // The queue of received messages.

    // Constructor initializes the subscriber with an ID.
    MessageSubscriber(size_t id) : id(id) {}

    // Receives a message if it is addressed to this subscriber.
    void receiveMessage(const Message& msg) {
        if (msg.recipientId == id) {
            queue.enqueue(msg);
        }
    }

    // Processes all messages in the queue.
    void processMessages() {
        while (!queue.isEmpty()) {
            try {
                Message msg = queue.dequeue();
                std::cout << "Processing message: " << msg.content << " for subscriber: " << id << std::endl;
            } catch (const std::exception& e) {
                std::cerr << "Error: " << e.what() << std::endl;
            }
        }
    }
};

// AtomComposition class represents a specific type of atom composition.
class AtomComposition {
public:
    std::string type;         // The type of the atom composition.

    // Constructor initializes the atom composition with a type.
    AtomComposition(std::string type) : type(std::move(type)) {}
};

// LeadReadingPlan enum represents different plans or states.
enum class LeadReadingPlan {
    Explorationship,         // A plan for exploration.
    Arsenalship,             // A plan for arsenal.
    // Add more plans here...
};

// YiQiEntity class represents a generic entity.
class YiQiEntity {
public:
    std::string name;        // The name of the entity.

    // Constructor initializes the entity with a name.
    YiQiEntity(std::string name) : name(std::move(name)) {}

    // Prints information about the entity.
    void printInfo() const {
        std::cout << "YiQi Entity Name: " << name << std::endl;
    }

    // Stores data related to the entity.
    void storeData(const std::string& data) {
        std::cout << "Storing data: " << data << std::endl;
    }

    // Communicates a message.
    void communicate(const std::string& message) {
        std::cout << "Communicating message: " << message << std::endl;
    }

    // Performs computations.
    void compute() const {
        std::cout << "Computing..." << std::endl;
    }
};

// YiQiEntityVM class represents a virtual machine entity.
class YiQiEntityVM {
public:
    std::shared_ptr<YiQiEntity> entity;  // The associated YiQiEntity.
    LeadReadingPlan plan;                // The plan associated with the VM.

    // Constructor initializes the VM with an entity and a plan.
    YiQiEntityVM(std::shared_ptr<YiQiEntity> entity, LeadReadingPlan plan)
        : entity(std::move(entity)), plan(plan) {}

    // Prints information about the VM.
    void printInfo() const {
        entity->printInfo();
    }

    // Stores data using the associated entity.
    void storeData(const std::string& data) {
        std::cout << "YiQiEntityVM storing data: " << data << std::endl;
        entity->storeData(data);
    }

    // Communicates a message using the associated entity.
    void communicate(const std::string& message) {
        std::cout << "YiQiEntityVM communicating message: " << message << std::endl;
        entity->communicate(message);
    }

    // Performs computations using the associated entity.
    void compute() const {
        std::cout << "YiQiEntityVM computing..." << std::endl;
        entity->compute();
    }
};

// YiQiEntityVMAtomComposition class combines AtomComposition and YiQiEntityVM functionality.
class YiQiEntityVMAtomComposition : public MessageSubscriber {
public:
    AtomComposition composition;  // The atom composition.
    YiQiEntityVM vm;              // The virtual machine.

    // Constructor initializes the composition with an ID, atom type, entity, and plan.
    YiQiEntityVMAtomComposition(size_t id, std::string atomType, std::shared_ptr<YiQiEntity> entity, LeadReadingPlan plan)
        : MessageSubscriber(id), composition(std::move(atomType)), vm(std::move(entity), plan) {}

    // Prints information about the VM.
    void printVMInfo() const {
        vm.printInfo();
    }

    // Stores data using the associated VM.
    void storeData(const std::string& data) {
        std::cout << "Storing data: " << data << " for AtomComposition of type: " << composition.type << std::endl;
        vm.storeData(data);
    }

    // Communicates a message using the associated VM.
    void communicate(const std::string& message) {
        std::cout << "Communicating message: " << message << " for AtomComposition of type: " << composition.type << std::endl;
        vm.communicate(message);
    }

    // Performs computations using the associated VM.
    void compute() const {
        std::cout << "Computing for AtomComposition of type: " << composition.type << std::endl;
        vm.compute();
    }
};

// YiQiEntityVMManager class manages multiple YiQiEntityVMAtomComposition instances.
class YiQiEntityVMManager {
public:
    std::unordered_map<size_t, std::shared_ptr<YiQiEntityVMAtomComposition>> subscribers;  // Map of subscribers.
    MessageQueue globalQueue;  // Global message queue.

    // Adds a subscriber to the manager.
    void addSubscriber(std::shared_ptr<YiQiEntityVMAtomComposition> comp) {
        subscribers[comp->id] = std::move(comp);
    }

    // Removes a subscriber from the manager.
    void removeSubscriber(size_t id) {
        subscribers.erase(id);
    }

    // Gets a subscriber by ID.
    std::shared_ptr<YiQiEntityVMAtomComposition> getSubscriber(size_t id) const {
        auto it = subscribers.find(id);
        if (it != subscribers.end()) {
            return it->second;
        }
        return nullptr;
    }

    // Sends a message to the global queue.
    void sendMessage(const Message& msg) {
        globalQueue.enqueue(msg);
    }

    // Processes messages in the global queue.
    void processGlobalMessages() {
        while (!globalQueue.isEmpty()) {
            try {
                Message msg = globalQueue.dequeue();
                auto subscriber = getSubscriber(msg.recipientId);
                if (subscriber) {
                    subscriber->receiveMessage(msg);
                } else {
                    std::cerr << "Recipient not found for message: " << msg.content << std::endl;
                }
            } catch (const std::exception& e) {
                std::cerr << "Error processing global messages: " << e.what() << std::endl;
            }
        }
    }

    // Performs a compute operation on all subscribers.
    void globalCompute() const {
        for (const auto& [id, comp] : subscribers) {
            comp->compute();
        }
    }

    // Stores data on all subscribers.
    void globalStoreData(const std::string& data) {
        for (const auto& [id, comp] : subscribers) {
            comp->storeData(data);
        }
    }

    // Communicates a message to all subscribers.
    void globalCommunicate(const std::string& message) {
        for (const auto& [id, comp] : subscribers) {
            comp->communicate(message);
        }
    }
};

// Example usage
int main() {
    // Create a YiQiEntity instance
    auto entity = std::make_shared<YiQiEntity>("Space Explorer");

    // Create a YiQiEntityVMAtomComposition instance
    auto composition1 = std::make_shared<YiQiEntityVMAtomComposition>(1, "Carbon", entity, LeadReadingPlan::Explorationship);

    // Create a YiQiEntityVMManager instance
    YiQiEntityVMManager manager;
    manager.addSubscriber(composition1);

    // Send a message to the global queue
    manager.sendMessage(Message("Hello, world!", composition1->id));

    // Process global messages
    manager.processGlobalMessages();

    // Perform global operations
    manager.globalCompute();
    manager.globalStoreData("Global Data");
    manager.globalCommunicate("Global Communication");

    return 0;
}
```

