|  ALLinOnepackage | ALLinOnepackage  |
|---|---|
|  ALLinOnepage |ack   |


# ALLinOnepackage 

MCP是什么？
MCP，即Multi-Chip-Package简称，中文意思是多制层封装芯片，即记忆体NOR Flash、NAND Flash、Low Power SRAM及Pseudo SRAM等堆叠封装成一颗的多晶片。其主要应用领域为手机等手持智能终端设备，优点在于体积小、成本低，能适应各种手持设备节省空间的原则，同时比独立的芯片组合价格更加低廉。

对于其他的电子产品，应该常见于单独的芯片设计；手机主要是PCB面积太小了，无法使用单独的芯片组和。

MCP技术发展的关键在于封装厚度的控制及测试的问题。一般来说，MCP所堆叠的记忆体晶片数量愈多，它的厚度也将随之增加，所以在整个设计过程中需控制晶片的厚度以减少晶片堆叠的空间。

# 芯片只是堆叠方式发生了变化，具体的接口并没有改变，可以说只是外形发生了变化。