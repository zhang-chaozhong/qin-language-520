class AttentionMechanism:  
    def apply_attention(self, inputs, context=None):  
        """  
        应用注意力机制到输入数据上。  
          
        Args:  
            inputs (Tensor): 输入数据  
            context (Tensor, optional): 上下文信息，默认为None  
          
        Returns:  
            Tensor: 注意力处理后的输出  
        """  
        raise NotImplementedError("子类需要实现这个方法")

@AttentionMechanismRegistry.register("multi_head")  
class MultiHeadAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 允许模型同时关注不同的信息子空间  
        pass  

@AttentionMechanismRegistry.register("Adaptive")    
class AdaptiveAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 动态调整注意力的聚焦点  
        pass

@AttentionMechanismRegistry.register("Local")  
class LocalAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 关注序列中的局部区域以提高效率  
        pass  

@AttentionMechanismRegistry.register("Global")    
class GlobalAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 在整个序列上计算注意力  

@AttentionMechanismRegistry.register("Hierarchical")  
class HierarchicalAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 多级别的注意力机制，适用于复杂结构  
        pass  

@AttentionMechanismRegistry.register("Cross-")    
class Cross-Attention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 在不同模态或流程间共享注意力  
        pass

@AttentionMechanismRegistry.register("Self-")  
class Self-Attention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 一个序列内部元素间的注意力机制 
        pass  
  
@AttentionMechanismRegistry.register("Sparse")  
class SparseAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 只关注重要的键值对，提高效率。
        pass  

@AttentionMechanismRegistry.register("Convolutional")  
class ConvolutionalAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 结合卷积操作以捕捉局部模式。  
        pass  
  
@AttentionMechanismRegistry.register("Gated")  
class GatedAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 通过门控机制控制信息流。
        pass

@AttentionMechanismRegistry.register("Adversarial")  
class AdversarialAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 使用对抗训练来改善注意力的鲁棒性。  
        pass  
  
@AttentionMechanismRegistry.register("Graph")  
class GraphAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 用于处理图结构数据。
        pass  

@AttentionMechanismRegistry.register("Hard")  
class HardAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 基于离散选择，而非软性权重分配。
        pass  

@AttentionMechanismRegistry.register("Soft")    
class SoftAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 连续且可微的注意力分配。 
        pass

@AttentionMechanismRegistry.register("Transformer-XL")  
class Transformer-XL(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 增强对长期依赖的捕捉。 
        pass  
  
@AttentionMechanismRegistry.register("BERT")  
class BERT(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 在所有层中结合左右两侧的上下文。
         pass  

@AttentionMechanismRegistry.register("Hybrid")  
class HybridAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 结合不同类型的注意力机制。
        pass  

@AttentionMechanismRegistry.register("Co-")    
class Co-Attention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 同时在两个相关序列上应用注意力。
        pass

@AttentionMechanismRegistry.register("Axial")  
class AxialAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 沿特定维度应用注意力，用于高维数据。
        pass  

@AttentionMechanismRegistry.register("FrequencyDomain")    
class FrequencyDomainAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 在频域内应用注意力。
        pass  

@AttentionMechanismRegistry.register("Distillation")  
class AttentionDistillation(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 从一个模型到另一个模型转移注意力模式。
        pass  
  
@AttentionMechanismRegistry.register("Pooling")  
class AttentionPooling(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 利用注意力权重进行特征池化。
        pass

@AttentionMechanismRegistry.register("Memory-Augmented")  
class Memory-AugmentedAttention(AttentionMechanism):  
    def apply_attention(self, inputs, context=None):  
        # 引入外部记忆机制以增强注意力。 
        pass  
  

class AttentionMechanismRegistry:  
    _registry = {}  
  
    @staticmethod  
    def register(name):  
        """  
        装饰器，用于注册注意力机制模型。  
        """  
        def decorator(cls):  
            AttentionMechanismRegistry._registry[name] = cls  
            return cls  
        return decorator  
  
    @staticmethod  
    def get(name):  
        """  
        根据名称获取注意力机制模型类。  
          
        Args:  
            name (str): 注意力机制模型的名称  
          
        Returns:  
            AttentionMechanism: 注意力机制模型类  
          
        Raises:  
            KeyError: 如果找不到对应的模型  
        """  
        return AttentionMechanismRegistry._registry[name]  
  
# 使用装饰器注册注意力机制模型  
@AttentionMechanismRegistry.register("multi_head")  
class MultiHeadAttention(AttentionMechanism):  
    # 类的实现同上  
  
@AttentionMechanismRegistry.register("self")  
class SelfAttention(AttentionMechanism):  
    # 类的实现同上  
  
# 示例：获取注册的注意力机制模型  
attention_mechanism = AttentionMechanismRegistry.get("multi_head")()
        pass




# 1.多头注意力(Multi-Head Attention):

# 允许模型同时关注不同的信息子空间。

.

# 2.自适应注意力(AdaptiveAttention):

# 动态调整注意力的聚焦点。

.

# 3.局部注意力(LocalAttention):

# 关注序列中的局部区域以提高效率。

.

# 4.全局注意力(GlobalAttention):

# 在整个序列上计算注意力。

.

# 5.层次注意力(HierarchicalAttention):

# 多级别的注意力机制，适用于复杂结构。

.

# 6.交叉注意力(Cross-Attention):

# 在不同模态或流程间共享注意力。

.
# 7.自注意力(Self-Attention):

# 一个序列内部元素间的注意力机制。

.

# 8.稀疏注意力(Sparse Attention):

# 只关注重要的键值对，提高效率。

.

# 9.卷积注意力(Convolutional Attention):

# 结合卷积操作以捕捉局部模式。

.

# 10.门控注意力(Gated Attention):

# 通过门控机制控制信息流。

.

# 11.对抗性注意力(AdversarialAttention):

# 使用对抗训练来改善注意力的鲁棒性。

.

# 12.图注意力(Graph Attention):

# 用于处理图结构数据。

.

# 13.硬注意力(Hard Attention):

# 基于离散选择，而非软性权重分配。

.

# 14.软注意力(SoftAttention):

# 连续且可微的注意力分配。

.

# 15.Transformer-XL的段级重复注意力:

# 增强对长期依赖的捕捉。

.

# 16.BERT的双向注意力:

# 在所有层中结合左右两侧的上下文。

.

# 17.混合注意力(Hybrid Attention):

# 结合不同类型的注意力机制。

.

# 18.协同注意力(Co-Attention):

# 同时在两个相关序列上应用注意力。

.

# 19.轴向注意力(AxialAttention):

# 沿特定维度应用注意力，用于高维数据。

.

# 20.频域注意力(Frequency Domain Attention):

# 在频域内应用注意力。

.

# 21.注意力蒸馏(Attention Distillation):

# 从一个模型到另一个模型转移注意力模式。

.

# 22.注意力池化(Attention Pooling):

# 利用注意力权重进行特征池化。

.

# 23.记忆增强注意力(Memory-Augmented Attention):
# 引入外部记忆机制以增强注意力。