classDiagram
    class Image {
        -id: str
        -data: bytes
        +init(id: str, data: bytes): None
    }

    class ImageRepository {
        -images: dict[str, Image]
        +init(): None
        +add_image(image: Image): None
        +get_image(image_id: str): Image | None
    }

    class ClientHandler {
        -server: Server
        -client_socket: socket.socket
        +init(server: Server, client_socket: socket.socket): None
        +run(): None
        +send_image(image: Image): None
    }

    class Server {
        -host: str
        -port: int
        -client_handlers: list[ClientHandler]
        -image_repository: ImageRepository
        +init(host: str = '127.0.0.1', port: int = 12345): None
        +start(): None
    }

    class CommunityMember {
        -name: str
        -skills: list[str]
        +init(name: str, skills: list[str]): None
        +contribute_knowledge(knowledge: str): None
    }

    class Community {
        -name: str
        -members: list[CommunityMember]
        -shared_knowledge: set[str]
        +init(name: str): None
        +add_member(member: CommunityMember): None
        +collect_knowledge(): None
        +display_knowledge_intersection(): None
    }

    class EconomicPlan {
        -community: Community
        +init(community: Community): None
    }

    class SignalProcessingEngine {
        +static pcm_encode(analog_signal: list[float], bits: int = 8): list[int]
        +static pcm_decode(digital_signal: list[int], bits: int = 8): list[float]
        +static psk_modulate(digital_signal: list[int], phase_shifts: list[float]): list[float]
    }

    class SignalSimulator {
        +generate_analog_signal(length: int = 10, amplitude_range: tuple[float, float] = (0, 1)): list[float]
        +simulate_processing(bits: int = 8, phase_shifts: list[float] = [0, np.pi/2, np.pi, 3*np.pi/2]): None
    }

    class SignalTester {
        +test_pcm_encoding(analog_signal: list[float], bits: int = 8): None
        +test_psk_modulation(digital_signal: list[int], phase_shifts: list[float]): None
    }

    ClientHandler ..> Server: has
    ImageRepository ..> Server: has
    CommunityMember ..> Community: has
    EconomicPlan ..> Community: has
    SignalSimulator ..o> SignalProcessingEngine: uses
    SignalTester ..o> SignalProcessingEngine: uses
    YiqiVirtualMachine ..> SignalProcessingEngine: extends
    YiqiVirtualMachine ..o> SignalSimulator: has
    YiqiVirtualMachine ..o> SignalTester: has