import numpy as np

class SignalProcessingEngine:
    @staticmethod
    def pcm_encode(analog_signal, bits=8):
        """PCM编码模拟信号到数字信号"""
        # 假设analog_signal是一个归一化到[0, 1]的浮点数列表
        # 这里简化处理，直接按比例映射到整数
        max_value = 2**bits - 1
        return [int(value * max_value) for value in analog_signal]

    @staticmethod
    def pcm_decode(digital_signal, bits=8):
        """PCM解码数字信号到模拟信号"""
        max_value = 2**bits - 1
        return [value / max_value for value in digital_signal]

    @staticmethod
    def psk_modulate(digital_signal, phase_shifts):
        """PSK调制，这里简化处理，仅返回相位偏移的索引"""
        # 假设phase_shifts是一个包含可能相位的列表
        # 这里直接返回相位偏移的索引作为模拟
        return [phase_shifts[digit % len(phase_shifts)] for digit in digital_signal]