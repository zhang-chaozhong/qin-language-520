import numpy as np

class SignalSimulator:
    def generate_analog_signal(self, length=10, amplitude_range=(0, 1)):
        # 生成模拟信号
        return np.random.uniform(*amplitude_range, length).tolist()