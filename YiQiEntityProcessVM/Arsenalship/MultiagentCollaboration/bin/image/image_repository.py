class ImageRepository:
    def __init__(self):
        self.images = {}

    def add_image(self, image):
        self.images[image.id] = image

    def get_image(self, image_id):
        return self.images.get(image_id)