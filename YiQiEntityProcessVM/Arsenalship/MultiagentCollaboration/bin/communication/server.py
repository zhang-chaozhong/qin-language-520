import socket
from .client_handler import ClientHandler
from ..image.image_repository import ImageRepository
from ..image.image import Image

class Server:
    def __init__(self, host='127.0.0.1', port=12345):
        self.host = host
        self.port = port
        self.client_handlers = []
        self.image_repository = ImageRepository()
        # 示例：添加一些镜像
        self.image_repository.add_image(Image("test_image", b"This is a test image data"))

    def start(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host, self.port))
            s.listen()
            print(f"Server listening on {self.host}:{self.port}")
            while True:
                conn, addr = s.accept()
                print(f"Connected by {addr}")
                handler = ClientHandler(self, conn)
                self.client_handlers.append(handler)
                handler.start()