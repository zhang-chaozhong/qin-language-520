import socket
from threading import Thread

class ClientHandler(Thread):
    def __init__(self, server, client_socket):
        super().__init__()
        self.server = server
        self.client_socket = client_socket

    def run(self):
        # 接收请求，这里省略了具体请求处理逻辑
        # 假设客户端请求了一个镜像ID
        image_id = "test_image"
        image = self.server.image_repository.get_image(image_id)
        if image:
            self.send_image(image)
        else:
            self.client_socket.sendall(b"Image not found")
        self.client_socket.close()

    def send_image(self, image):
        # 发送镜像数据，这里简单模拟
        self.client_socket.sendall(image.data)