#include <iostream>  // 引入标准输入输出流库，用于输出信息到控制台  
#include <string>    // 引入字符串库，用于处理字符串  
  
// 定义一个枚举类型，用于表示微生物之间的不同关系  
enum class MicrobeRelationshipType {    
    Coexistence,  // 共存关系  
    Mutualism,    // 互惠共生关系  
    Symbiosis,    // 共生关系（广义，包括互惠共生和偏利共生）  
    Antagonism,   // 拮抗关系  
    Competition,  // 竞争关系  
    Parasitism,   // 寄生关系  
    Predation     // 捕食关系  
};  
  
// 定义一个微生物基类，用于表示不同类型的微生物  
class Microbe {    
public:    
    std::string name;  // 微生物的名称  
    
    // 构造函数，初始化微生物的名称  
    Microbe(std::string name) : name(name) {}    
    
    // 虚析构函数，确保通过基类指针删除派生类对象时能够正确调用派生类的析构函数  
    virtual ~Microbe() {}    
    
    // 虚函数，用于打印微生物的信息  
    // 使用virtual关键字，使得派生类可以覆盖此函数以提供特定于派生类的实现  
    virtual void printInfo() const {    
        std::cout << "Microbe: " << name << std::endl;    
    }    
};  
  
// 定义一个模板类，用于表示微生物之间的关系  
// 使用模板以支持不同类型的微生物之间的关系  
template<typename T, typename U>    
class MicrobeRelationship {    
protected:    
    T* first;  // 指向第一个微生物的指针  
    U* second; // 指向第二个微生物的指针  
    MicrobeRelationshipType type;  // 微生物之间的关系类型  
    
public:    
    // 构造函数，初始化两个微生物指针和关系类型  
    MicrobeRelationship(T* first, U* second, MicrobeRelationshipType type)    
        : first(first), second(second), type(type) {}    
    
    // 虚析构函数，虽然不是严格必需的（因为没有动态分配的资源），但保持一致性是个好习惯  
    virtual ~MicrobeRelationship() {}    
    
    // 成员函数，用于打印微生物之间的关系信息  
    void printRelationship() const {    
        // 使用条件运算符（?:）来根据关系类型输出相应的字符串  
        std::cout << "Relationship Type: "     
                  << (type == MicrobeRelationshipType::Coexistence ? "Coexistence"     
                  : type == MicrobeRelationshipType::Mutualism ? "Mutualism"    
                  : type == MicrobeRelationshipType::Symbiosis ? "Symbiosis"    
                  : type == MicrobeRelationshipType::Antagonism ? "Antagonism"    
                  : type == MicrobeRelationshipType::Competition ? "Competition"    
                  : type == MicrobeRelationshipType::Parasitism ? "Parasitism"    
                  : type == MicrobeRelationshipType::Predation ? "Predation"    
                  : "Unknown")    
                  << " between " << first->name << " and " << second->name << std::endl;    
    }    
};