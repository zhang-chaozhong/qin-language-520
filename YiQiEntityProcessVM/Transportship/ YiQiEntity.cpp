#include <iostream>  
#include <string>  
#include <memory>  
  
// 属性类  
class PhysicalCharacteristic {  
public:  
    std::string description;  
    PhysicalCharacteristic(const std::string& desc) : description(desc) {}  
};  
  
class GeographicInfo {  
public:  
    std::string location;  
    GeographicInfo(const std::string& loc) : location(loc) {}  
};  
  
// 省略其他属性类（如 PhysiologicalState, PsychologicalProfile 等）的实现  
  
// 实体基类  
class YiQiEntity {  
public:  
    std::unique_ptr<PhysicalCharacteristic> physicalChar;  
    std::unique_ptr<GeographicInfo> geoInfo;  
    // 可以添加更多属性  
  
    YiQiEntity(const std::string& physicalDesc, const std::string& location)  
        : physicalChar(std::make_unique<PhysicalCharacteristic>(physicalDesc)),  
          geoInfo(std::make_unique<GeographicInfo>(location)) {}  
  
    // 虚析构函数确保正确删除派生类对象  
    virtual ~YiQiEntity() = default;  
};  
  
// 特定实体类，包含地理信息的引用  
class YiQiEntityGeoInfo : public YiQiEntity {  
public:  
    std::shared_ptr<GeographicInfo> sharedGeoInfo; // 使用共享指针以便被多个对象共享  
  
    YiQiEntityGeoInfo(const std::string& physicalDesc, const std::string& location)  
        : YiQiEntity(physicalDesc, location),  
          sharedGeoInfo(std::make_shared<GeographicInfo>(location)) {}  
};  
  
// 实体别名类，包含对多种实体的引用  
class YiQiEntityAlias {  
public:  
    std::unique_ptr<YiQiEntity> person;  
    std::unique_ptr<YiQiEntity> organization;  
    std::vector<std::unique_ptr<YiQiEntityGeoInfo>> geoInfos; // 假设可以包含多个地理信息实体  
  
    // 构造函数示例  
    YiQiEntityAlias(std::unique_ptr<YiQiEntity> p, std::unique_ptr<YiQiEntity> o)  
        : person(std::move(p)), organization(std::move(o)) {}  
  
    // 添加地理信息的方法  
    void addGeoInfo(std::unique_ptr<YiQiEntityGeoInfo> info) {  
        geoInfos.push_back(std::move(info));  
    }  
};  
  
int main() {  
    auto person = std::make_unique<YiQiEntity>("Tall and Strong", "New York");  
    auto organization = std::make_unique<YiQiEntity>("Non-Profit", "San Francisco");  
  
    auto geoInfoPerson = std::make_unique<YiQiEntityGeoInfo>("Tall and Strong", "Manhattan");  
    auto geoInfoOrg = std::make_unique<YiQiEntityGeoInfo>("Non-Profit", "Downtown SF");  
  
    YiQiEntityAlias alias(std::move(person), std::move(organization));  
    alias.addGeoInfo(std::move(geoInfoPerson));  
    alias.addGeoInfo(std::move(geoInfoOrg));  
  
    // 这里可以添加更多代码来访问或展示对象信息  
  
    return 0;  
}