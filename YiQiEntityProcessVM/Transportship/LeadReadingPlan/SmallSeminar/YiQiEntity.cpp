#include <iostream>
#include <string>
#include <vector>

### 枚举定义

```cpp
enum class LeadReadingPlan {
    Transportship,
    Arsenalship,
    Explorationship
};
```

### 类定义

#### `Transportship` 类
```cpp
class Transportship {
public:
    void print() const { std::cout << "Transportship" << std::endl; }
};
```

#### `Arsenalship` 类
```cpp
class Arsenalship {
public:
    void print() const { std::cout << "Arsenalship" << std::endl; }
};
```

#### `Explorationship` 类
```cpp
class Explorationship {
public:
    void print() const { std::cout << "Explorationship" << std::endl; }
};
```

### 工厂函数
```cpp
std::unique_ptr<Transportship> createTransportship() {
    return std::make_unique<Transportship>();
}

std::unique_ptr<Arsenalship> createArsenalship() {
    return std::make_unique<Arsenalship>();
}

std::unique_ptr<Explorationship> createExplorationship() {
    return std::make_unique<Explorationship>();
}

std::unique_ptr<Transportship> createShip(LeadReadingPlan plan) {
    switch (plan) {
        case LeadReadingPlan::Transportship:
            return createTransportship();
        case LeadReadingPlan::Arsenalship:
            return createArsenalship();
        case LeadReadingPlan::Explorationship:
            return createExplorationship();
        default:
            return nullptr;
    }
}
```

### 打印函数
```cpp
void printShip(const std::unique_ptr<Transportship>& ship) {
    if (ship) {
        ship->print();
    } else {
        std::cout << "Unknown ship type" << std::endl;
    }
}
```

### 示例用法
```cpp
int main() {
    auto ship1 = createShip(LeadReadingPlan::Transportship);
    auto ship2 = createShip(LeadReadingPlan::Explorationship);
    printShip(ship1);
    printShip(ship2);
    return 0;
}
```

### 其他类

#### `YiQiEntity` 类
```cpp
class YiQiEntity {
public:
    std::string name;
    YiQiEntity(std::string name) : name(name) {}
    void print() const {
        std::cout << "YiQiEntity Name: " << name << std::endl;
    }
};
```

#### `YiQiEntityVM` 类
```cpp
class YiQiEntityVM {
public:
    std::shared_ptr<YiQiEntity> entity;
    LeadReadingPlan plan;
    YiQiEntityVM(std::shared_ptr<YiQiEntity> e, LeadReadingPlan p)
        : entity(std::move(e)), plan(p) {}
    void printInfo() const {
        std::cout << "Lead Reading Plan: ";
        switch (plan) {
            case LeadReadingPlan::Transportship:
                std::cout << "Transportship";
                break;
            case LeadReadingPlan::Arsenalship:
                std::cout << "Arsenalship";
                break;
            case LeadReadingPlan::Explorationship:
                std::cout << "Explorationship";
                break;
        }
        std::cout << ", YiQiEntity Name: ";
        if (entity) {
            entity->print();
        } else {
            std::cout << "None";
        }
        std::cout << std::endl;
    }
};
```

#### `YiQiEntityVMAtomComposition` 类
```cpp
class YiQiEntityVMAtomComposition {
public:
    YiQiEntityVM vm;
    YiQiEntityVMAtomComposition(std::shared_ptr<YiQiEntity> entity, LeadReadingPlan plan)
        : vm(std::move(entity), plan) {}
    void printVMInfo() const {
        vm.printInfo();
    }
};
```

### 示例用法
```cpp
int main() {
    auto entity = std::make_shared<YiQiEntity>("Space Explorer");
    YiQiEntityVMAtomComposition composition(entity, LeadReadingPlan::Explorationship);
    composition.printVMInfo();
    return 0;
}
```

### 其他类和结构

#### `AtomComposition` 类
```cpp
class AtomComposition {
public:
    std::string type;
    AtomComposition(std::string type) : type(type) {}
};
```

#### `E3Group` 类
```cpp
class E3Group {
public:
    std::vector<AtomComposition> compositions;
    std::string leadReadingPlan;
    std::string YiQiEntity;
    std::string YiQiEntityVM;

    E3Group(std::vector<AtomComposition> compositions, std::string plan)
        : compositions(compositions), leadReadingPlan(plan), YiQiEntity(""), YiQiEntityVM("") {}

    E3Group(std::vector<AtomComposition> compositions)
        : compositions(compositions), leadReadingPlan(""), YiQiEntity(""), YiQiEntityVM("") {}

    void setLeadReadingPlan(std::string plan) {
        leadReadingPlan = plan;
    }

    std::string getLeadReadingPlan() const {
        return leadReadingPlan;
    }

    void setYiQiEntity(std::string entity) {
        YiQiEntity = entity;
    }

    std::string getYiQiEntity() const {
        return YiQiEntity;
    }

    void setYiQiEntityVM(std::string vm) {
        YiQiEntityVM = vm;
    }

    std::string getYiQiEntityVM() const {
        return YiQiEntityVM;
    }
};
```

#### `YiQiEntityTriangleEdge` 类
```cpp
class YiQiEntityTriangleEdge {
public:
    double length;
    YiQiEntityTriangleEdge(double length) : length(length) {}
    void printLength() const {
        std::cout << "Edge length: " << length << std::endl;
    }
    void setLength(double newLength) {
        length = newLength;
    }
    double getLengthSquared() const {
        return length * length;
    }
};
```

#### `YiQiEntityVMTriangleVertex` 类
```cpp
class YiQiEntityVMTriangleVertex {
public:
    std::string name;
    YiQiEntityVMTriangleVertex(std::string name) : name(name) {}
    void LeadReadingPlan() const {
        std::cout << "Leading reading plan for vertex: " << name << std::endl;
    }
};
```

template<typename T>
class QinObject {
public:
    T Arsenalship;
    T Explorationship;
    T Transportship;
    int LeadReadingPlan（Transportship，Arsenalship，Explorationship）;

    QinObject(T x, T y, T z, int credit) : Arsenalship(Arsenalship), Explorationship(Explorationship), Transportship(Transportship), LeadReadingPlan(LeadReadingPlan) {}

    // 定义结构体
typedef struct {
    T Arsenalship;
    T Explorationship;
    T Transportship;
    int LeadReadingPlan（Transportship，Arsenalship，Explorationship）;
} QinObject;


// 将 Python 中的 SpaceObject 实例转换为 C 语言的 QinObject
QinObject to_c_qin_object(SpaceObject *space_object) {
    QinObject obj;
    obj.x = space_object->x;
    obj.y = space_object->y;
    obj.z = space_object->z;
    obj.credit = space_object->credit;
    return obj;
}

QinObject to_c_qin_object(SpaceObject *space_object)
   