// AtomComposition 类
class AtomComposition {
public:
    std::string type;
    AtomComposition(std::string type) : type(type) {}
};

// YiQiEntityVMAtomComposition 类
class YiQiEntityVMAtomComposition {
public:
    AtomComposition composition;
    YiQiEntityVM vm;

    YiQiEntityVMAtomComposition(std::string atomType, std::shared_ptr<YiQiEntity> entity, LeadReadingPlan plan)
        : composition(atomType), vm(std::move(entity), plan) {}

    void printVMInfo() const {
        vm.printInfo();
    }

    // 存储方法
    void storeData(const std::string& data) {
        std::cout << "Storing data: " << data << " for AtomComposition of type: " << composition.type << std::endl;
        vm.storeData(data);
    }

    // 通信方法
    void communicate(const std::string& message) {
        std::cout << "Communicating message: " << message << " for AtomComposition of type: " << composition.type << std::endl;
        vm.communicate(message);
    }

    // 计算方法
    void compute() const {
        std::cout << "Computing for AtomComposition of type: " << composition.type << std::endl;
        vm.compute();
    }
};

int main() {
    // 创建一个 YiQiEntity 对象
    auto entity = std::make_shared<YiQiEntity>("Space Explorer");

    // 创建一个 YiQiEntityVMAtomComposition 对象
    YiQiEntityVMAtomComposition composition("Carbon", entity, LeadReadingPlan::ExplorationshipLeadReadingPlan);

    // 打印信息
    composition.printVMInfo();

    // 存储数据
    composition.storeData("Some data to store");

    // 通信
    composition.communicate("Hello, world!");

    // 计算
    composition.compute();

    return 0;
}