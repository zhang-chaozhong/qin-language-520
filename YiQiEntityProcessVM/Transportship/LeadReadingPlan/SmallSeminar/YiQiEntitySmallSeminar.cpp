#include <iostream>
#include <memory>

// YiQiEntity 类
class YiQiEntity {
public:
    std::string name;
    YiQiEntity(std::string name) : name(name) {}
    virtual void print() const {
        std::cout << "YiQiEntity Name: " << name << std::endl;
    }
};

// YiQiEntityVM 类
class YiQiEntityVM {
public:
    std::shared_ptr<YiQiEntity> entity;
    LeadReadingPlan plan;

    YiQiEntityVM(std::shared_ptr<YiQiEntity> e, LeadReadingPlan p)
        : entity(std::move(e)), plan(p) {}

    void printInfo() const {
        std::cout << "Lead Reading Plan: ";
        switch (plan) {
            case LeadReadingPlan::TransportshipLeadReadingPlan:
                std::cout << "Transportship";
                break;
            case LeadReadingPlan::ArsenalshipLeadReadingPlan:
                std::cout << "Arsenalship";
                break;
            case LeadReadingPlan::ExplorationshipLeadReadingPlan:
                std::cout << "Explorationship";
                break;
        }
        std::cout << ", YiQiEntity Name: ";
        if (entity) {
            entity->print();
        } else {
            std::cout << "None";
        }
        std::cout << std::endl;
    }

    // 存储方法
    void storeData(const std::string& data) {
        std::cout << "Storing data: " << data << " for VM with plan: ";
        switch (plan) {
            case LeadReadingPlan::TransportshipLeadReadingPlan:
                std::cout << "Transportship";
                break;
            case LeadReadingPlan::ArsenalshipLeadReadingPlan:
                std::cout << "Arsenalship";
                break;
            case LeadReadingPlan::ExplorationshipLeadReadingPlan:
                std::cout << "Explorationship";
                break;
        }
        std::cout << std::endl;
    }

    // 通信方法
    void communicate(const std::string& message) {
        std::cout << "Communicating message: " << message << " from VM with plan: ";
        switch (plan) {
            case LeadReadingPlan::TransportshipLeadReadingPlan:
                std::cout << "Transportship";
                break;
            case LeadReadingPlan::ArsenalshipLeadReadingPlan:
                std::cout << "Arsenalship";
                break;
            case LeadReadingPlan::ExplorationshipLeadReadingPlan:
                std::cout << "Explorationship";
                break;
        }
        std::cout << std::endl;
    }

    // 计算方法
    void compute() const {
        std::cout << "Computing for VM with plan: ";
        switch (plan) {
            case LeadReadingPlan::TransportshipLeadReadingPlan:
                std::cout << "Transportship";
                break;
            case LeadReadingPlan::ArsenalshipLeadReadingPlan:
                std::cout << "Arsenalship";
                break;
            case LeadReadingPlan::ExplorationshipLeadReadingPlan:
                std::cout << "Explorationship";
                break;
        }
        std::cout << std::endl;
    }
};