#include <iostream>  
#include <vector>  
#include <string>  
  
// 家族类  
class Family {  
public:  
    std::string name;  
  
    Family(std::string name) : name(name) {}  
  
    void printInfo() {  
        std::cout << "Family: " << name << std::endl;  
    }  
};  
  
// 军团类  
class Corps {  
public:  
    std::string name;  
  
    Corps(std::string name) : name(name) {}  
  
    void printInfo() {  
        std::cout << "Corps: " << name << std::endl;  
    }  
};  
  
// 联盟类  
class Alliance {  
private:  
    std::vector<Corps> corpsList;  
    std::vector<Family> familyList;  
  
public:  
    // 添加军团  
    void addCorps(const Corps& corps) {  
        corpsList.push_back(corps);  
    }  
  
    // 添加家族  
    void addFamily(const Family& family) {  
        familyList.push_back(family);  
    }  
  
    // 打印所有军团和家族信息  
    void printInfo() {  
        std::cout << "Alliance Components:" << std::endl;  
        for (const auto& corps : corpsList) {  
            corps.printInfo();  
        }  
        for (const auto& family : familyList) {  
            family.printInfo();  
        }  
    }  
};  
  
int main() {  
    // 创建联盟对象  
    Alliance myAlliance;  
  
    // 向联盟中添加军团和家族  
    myAlliance.addCorps(Corps("Corps1"));  
    myAlliance.addCorps(Corps("Corps2"));  
    myAlliance.addFamily(Family("Family1"));  
    myAlliance.addFamily(Family("Family2"));  
  
    // 打印联盟信息  
    myAlliance.printInfo();  
  
    return 0;  
}

//首先，我们定义Family（家族）类、Corps（军团）类和Alliance（联盟）类。
//其中，Alliance类将包含Corps和Family的实例或列表，具体取决于你的需求（这里我假设每个联盟可以有多个军团和家族）。
//通过组合类（Class）和继承（Inheritance）或者聚合（Aggregation）的方式来实现。
//使用聚合（特别是组合）的方式来向一个“联盟”对象中添加“军团”组件和“家族”组件的示例。