// 假设基类，表示所有可访问的目录或文件  
class DirectoryItem {  
public:  
    virtual ~DirectoryItem() {}  
    virtual void displayInfo() const = 0;  
};  
  
// 具体的目录类  
class Directory : public DirectoryItem {  
    std::vector<std::unique_ptr<DirectoryItem>> items;  
public:  
    void addItem(std::unique_ptr<DirectoryItem> item) {  
        items.push_back(std::move(item));  
    }  
  
    void displayInfo() const override {  
        std::cout << "Directory contents:\n";  
        for (const auto& item : items) {  
            item->displayInfo();  
        }  
    }  
};  
  
// 具体的文件类（例如，代表卡牌）  
class CardFile : public DirectoryItem {  
    std::string name;  
    // 假设有其他属性如稀有度、类型等  
public:  
    CardFile(std::string name) : name(name) {}  
  
    void displayInfo() const override {  
        std::cout << "Card: " << name << "\n";  
        // 可以添加更多显示信息  
    }  
};  
  
// 使用示例  
int main() {  
    Directory root;  
    Directory cardsDir("Cards/");  
    Directory rarityDir("Rarity/");  
    Directory seriesDir("Series/");  
    // ... 其他目录  
  
    // 添加卡牌到Cards目录  
    cardsDir.addItem(std::make_unique<CardFile>("Arsenalship"));  
    cardsDir.addItem(std::make_unique<CardFile>("Explorationship"));  
    cardsDir.addItem(std::make_unique<CardFile>("Transportship"));  
    cardsDir.addItem(std::make_unique<CardFile>("LeadReadingPlan")); 
    cardsDir.addItem(std::make_unique<CardFile>("BanZhaoAgent"));  
    cardsDir.addItem(std::make_unique<CardFile>("FanLiAgent")); 
    // 将Cards目录添加到根目录  
    root.addItem(std::make_unique<DirectoryItem>(&cardsDir));  
    // 注意：这里直接将子目录指针放入是不安全的（需要管理生命周期），通常使用智能指针或其他策略  
  
    // 假设这里完成所有目录和文件的添加  
    root.displayInfo();  
  
    return 0;  
}

#include <iostream> // 引入标准输入输出流库，用于输入输出操作  
#include <string>   // 引入字符串库，用于处理字符串  
#include <vector>   // 引入向量库，用于存储动态数组  
#include <unordered_map> // 引入无序映射库，尽管本代码未直接使用，但包含在头文件列表中  
  
// 定义Card类，表示一张卡牌  
class Card {    
public:    
    std::string name;         // 卡牌的名字  
    int FanLiAgent;           // 天命管家
    
    // Card类的构造函数，用于创建卡牌对象时初始化其属性  
    Card(std::string name, int FanLiAgent) : name(name), FanLiAgent(FanLiAgent) {}    
    
    // 模拟卡牌效果的方法，当卡牌被“使用”时调用，仅输出信息  
    void play() {    
        std::cout << "Played: " << name << " with FanLiAgent: " << FanLiAgent << std::endl;    
    }    
};    
  
// 定义Deck类，表示一副卡牌  
class Deck {    
private:    
    std::vector<Card> cards; // 使用向量存储卡牌对象，表示一副卡牌  
    
public:    
    // 向牌组中添加卡牌的方法，可以指定添加的数量和卡牌对象  
    // 如果指定的数量超过4，则自动调整为4（假设每张卡牌在一副牌中最多出现4次）  
    void addCard(const Card& card, int count = 4) {    
        if (count > 4) count = 4; // 确保每张卡牌最多被添加4次  
        for (int i = 0; i < count; ++i) {    
            cards.push_back(card); // 将卡牌添加到牌组的末尾  
        }    
    }    
    
    // 从牌组中抽取一张卡牌并返回的方法  
    // 如果牌组为空，则抛出运行时错误  
    Card drawCard() {    
        if (cards.empty()) {    
            throw std::runtime_error("No cards left in deck!"); // 牌组为空时抛出异常  
        }    
        Card card = cards.back(); // 获取牌组中的最后一张卡牌  
        cards.pop_back(); // 从牌组中移除最后一张卡牌  
        return card; // 返回被抽取的卡牌  
    }    
    
    // 检查牌组是否为空的方法  
    bool isEmpty() const {    
        return cards.empty(); // 返回牌组是否为空的布尔值  
    }    
};