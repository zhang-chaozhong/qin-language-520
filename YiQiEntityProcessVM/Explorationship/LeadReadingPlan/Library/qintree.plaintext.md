<code dialect=@SelfCultivationSimulator，name=qintree，value=XUNKUANG_NumberTheory>
<!--
/@SelfCultivationSimulator  
├── scripts/  
│   ├── pygame_client/  
│   │   ├── descriptors/  
│   │   │   ├── positions.py     # 包含通事、执事等职位的pygame客户端描述符  
│   │   │   ├── institutions.py  # 包含琴生生物机械科技工业研究所等机构的pygame客户端描述符  
│   │   ├── main_client.py       # pygame客户端主程序  
│   ├── python_server/  
│   │   ├── macros/  
│   │   │   ├── cpp_macros.h     # C++宏定义文件（如为己之学的C++宏名）  
│   │   ├── scripts/  
│   │   │   ├── service_scripts.py # 服务端脚本  
│   │   ├── main_server.py       # python服务端主程序  
│   ├── simulators/  
│   │   ├── pygame_simulator/  
│   │   │   ├── star_gate_fleet/  
│   │   │   │   ├── armory_ship.py  # 武库舰模拟器  
│   │   │   │   ├── exploration_ship.py # 勘探舰模拟器  
│   │   │   │   ├── transport_ship.py # 运输舰模拟器  
│   │   │   ├── main_simulator.py  # pygame模拟器主程序  
│   ├── editor/  
│   │   ├── SelfCultivationSimulator/  
│   │   │   ├── cpp_modules/        # 存放C++模块的代码  
│   │   │   │   ├── ConsulModule.cpp & ConsulModule.h  
│   │   │   │   ├── SupervisorModule.cpp & SupervisorModule.h  
│   │   │   │   ├── ExecutorGroupImpl.cpp & ExecutorGroupImpl.h  
│   │   │   │   ├── AdministratorGroupImpl.cpp & AdministratorGroupImpl.h  
│   │   │   ├── interfaces/         # 定义接口  
│   │   │   │   ├── CouncilService.h  
│   │   │   │   ├── ExecutorGroup.h  
│   │   │   │   ├── AdministratorGroup.h  
│   │   │   ├── implementations/    # 实现具体功能的模块（可选，取决于是否分离接口与实现）  
│   │   │   ├── models/             # 定义数据模型  
│   │   │   │   ├── JointMeeting.cpp & JointMeeting.h  
│   │   │   │   ├── BrainstormingSession.cpp & BrainstormingSession.h  
│   │   │   ├── utilities/          # 提供工具类  
│   │   │   │   ├── Logger.cpp & Logger.h  
│   │   │   │   ├── DatabaseAccess.cpp & DatabaseAccess.h  
│   │   │   │   ├── NetworkCommunicator.cpp & NetworkCommunicator.h  
│   │   │   ├── tests/              # 存放测试代码  
│   │   │   │   ├── test_ConsulModule.cpp  
│   │   │   │   ├── test_SupervisorModule.cpp  
│   │   │   │   ├── test_JointMeeting.cpp  
│   │   │   ├── CMakeLists.txt      # 项目的构建脚本  
│   │   │   ├── ...                 # 其他为己之学编辑器相关文件或目录  
│   │   ├── QINSHENG_BIOMECH/     # 琴生生物机械科技工业研究所编辑器相关文件  
│   │   ├── SHUDAO_ACADEMY/       # 书道学院编辑器相关文件  
│   │   ├── GUOYI_MEDICINE/       # 国医学院编辑器相关文件  
│   │   ├── DIJIU_ART_ACADEMY/    # 第九艺术学院编辑器相关文件  
│   │   ├── positions/            # 职位（如执事、通事）编辑器相关文件  
│   ├── cards/  
│   │   ├── positions/            # 职位卡牌信息（通事、执事等）  
│   │   ├── institutions/         # 机构卡牌信息（琴生生物机械科技工业研究所等）  
│   │   ├── staff/                # 教职人员卡牌信息（教员、教习、教职、教授等）  
│   ├── README.md                # 项目说明文件
-->
</code>