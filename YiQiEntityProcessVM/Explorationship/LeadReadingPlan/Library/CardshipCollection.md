ShipCardCollection/: 这是语言特性与语法结构卡牌集合的根目录。
Cards/: 包含所有语言谱系的目录。
Rarity/: 根据语言谱系的稀有度分类的目录，
Series/: 根据语言谱系所属的系列或版本分类的目录，
Type/: 根据语言谱系的类型分类的目录，
Special/: 包含特殊卡牌，如促销卡、限量版卡等的目录。
Templates/: 包含语言谱系模板文件的目录，这些模板定义了卡牌的基本结构和样式。
Metadata/: 包含语言谱系集合的元数据文件，如稀有度定义、系列信息等。
Schema/: 包含定义语言谱系数据结构的JSON Schema文件，确保卡牌数据的一致性和准确性。

ShipCardCollection/  # 计算机语言知识图谱的根目录（假设名字是特定语境下的命名）  
├── Cards/           # 包含所有计算机语言分类的目录  
│   ├── Programming/ # 编程语言目录  
│   │   ├── Python/  
│   │   │   ├── Python3.json  # Python 3.x 版本的语言特性卡牌  
│   │   │   └── ...  
│   │   ├── Java/  
│   │   │   ├── JavaSE.json    # Java SE（标准版）语言特性卡牌  
│   │   │   └── ...  
│   │   ├── JavaScript/  
│   │   │   ├── ES6.json       # ECMAScript 6 特性卡牌  
│   │   │   └── ...  
│   │   └── ...  
│   ├── Scripting/   # 脚本语言目录  
│   │   ├── Bash/  
│   │   │   ├── Basics.json    # Bash 基本语法卡牌  
│   │   │   └── ...  
│   │   ├── PowerShell/  
│   │   │   ├── Cmdlets.json   # PowerShell Cmdlets 卡牌  
│   │   │   └── ...  
│   │   └── ...  
│   ├── Markup/      # 标记语言目录  
│   │   ├── HTML/  
│   │   │   ├── HTML5.json     # HTML5 特性卡牌  
│   │   │   └── ...  
│   │   ├── XML/  
│   │   │   ├── Basics.json    # XML 基本语法卡牌  
│   │   │   └── ...  
│   │   └── ...  
│   ├── Query/       # 查询语言目录  
│   │   ├── SQL/  
│   │   │   ├── SQL92.json     # SQL-92 标准卡牌  
│   │   │   └── ...  
│   │   └── ...  
│   └── ...  
├── Rarity/          # 根据语言特性的稀有度（这里可能不太适用，但为保持一致性保留）  
│   ├── Core/        # 核心特性，类似于“常见”  
│   │   ├── PythonCore.json  
│   │   └── ...  
│   ├── Advanced/    # 高级特性，类似于“不常见”  
│   │   ├── JavaAdvanced.json  
│   │   └── ...  
│   ├── Experimental/# 实验性特性，类似于“稀有”  
│   │   ├── JavaScriptExperimental.json  
│   │   └── ...  
│   └── ...  
├── Series/          # 根据语言版本或标准系列分类  
│   ├── PythonSeries/  
│   │   ├── 2.x/  
│   │   │   ├── Python2.json  
│   │   │   └── ...  
│   │   ├── 3.x/  
│   │   │   ├── Python3.json  
│   │   │   └── ...  
│   │   └── ...  
│   ├── JavaScriptSeries/  
│   │   ├── ES5/  
│   │   │   ├── ES5.json  
│   │   │   └── ...  
│   │   ├── ES6/  
│   │   │   ├── ES6.json  
│   │   │   └── ...  
│   │   └── ...  
│   └── ...  
├── Type/            # 根据语言类型或用途分类（这里可能与Cards下的分类重叠）  
│   ├── GeneralPurpose/# 通用编程语言  
│   │   ├── Python.json  
│   │   ├── Java.json  
│   │   └── ...  
│   ├── Scripting/# 脚本语言  
│   │   ├── Bash.json  
│   │   ├── PowerShell.json  
│   │   └── ...  
│   ├── Markup/# 标记语言  
│   │   ├── HTML.json  
│   │   ├── XML.json  
│ │ └── ...
│ ├── Query/# 查询语言
│ │ ├── SQL.json
│ │ └── ...
│ └── ...
├── Special/ # 包含特殊卡牌，如特定用途、历史意义等
│ ├── Historic/ # 历史重要语言或版本
│ │ ├── Fortran.json # Fortran，作为早期编程语言的代表
│ │ └── ...
│ ├── Educational/ # 教育用途的特定语言或版本
│ │ ├── Scratch.json # Scratch，常用于儿童编程教育
│ │ └── ...
│ ├── LimitedEdition/# 限量版或特殊版本语言特性
│ │ ├── PythonLimited.json # 假设有Python的某个限量版特性
│ │ └── ...
│ └── ...
├── Templates/ # 包含定义卡牌基本结构和样式的模板文件
│ ├── CardTemplate.html # 通用卡牌HTML模板
│ ├── CardTemplate.json # 通用卡牌JSON结构模板
│ └── ...
├── Metadata/ # 包含语言谱系集合的元数据文件
│ ├── RarityDefinitions.json # 稀有度定义文件
│ ├── SeriesInfo.json # 系列信息文件
│ └── ...
├── Schema/ # 包含定义语言谱系数据结构的JSON Schema文件
│ ├── LanguageCardSchema.json # 语言卡牌数据结构定义


LanguageFamilyGraph.dot
```
digraph LanguageFamilyGraph {  
    rankdir=LR;  
    node [style=filled, color=lightgrey, fontname="Helvetica", fontsize=12];  
  
    Sino-Tibetan [shape=box, color=blue, fillcolor=lightblue, label="汉藏语系"];  
    Mandarin [shape=rectangle, color=black, fillcolor=white, label="汉语（普通话）"];  
  
    Sino-Tibetan -> Mandarin [label="属于", color=grey];  
  
    subgraph cluster_china {  
        label="中国语言";  
        color=red;  
        Mandarin;  
        Cantonese [shape=rectangle, label="粤语"];  
        Wu [shape=rectangle, label="吴语"];  
    }  
  
    // ... 其他节点和边  
}
```
## LanguageFamilyGraph.json
```
{  
    "graph": {  
        "name": "LanguageFamilyGraph",  
        "directed": true,  
        "rankdir": "LR",  // 布局方向，从左到右  
        "nodesep": "0.5", // 节点之间的间距  
        "ranksep": "1.0", // 层级之间的间距  
        "style": "filled",  
        "color": "lightgrey",  
        "fontname": "Helvetica",  
        "fontsize": "12"  
    },  
    "nodes": [  
        {  
            "id": "Sino-Tibetan",  
            "label": "汉藏语系",  
            "shape": "box",  
            "color": "blue",  
            "fillcolor": "lightblue"  
        },  
        {  
            "id": "Indo-European",  
            "label": "印欧语系",  
            "shape": "ellipse",  
            "color": "green",  
            "fillcolor": "lightgreen"  
        },  
        // ... 其他语系节点  
        {  
            "id": "Mandarin",  
            "label": "汉语（普通话）",  
            "shape": "rectangle",  
            "color": "black",  
            "fillcolor": "white"  
        },  
        // ... 其他语言节点  
    ],  
    "edges": [  
        {  
            "from": "Sino-Tibetan",  
            "to": "Mandarin",  
            "label": "属于",  
            "color": "grey"  
        },  
        // ... 其他边  
        {  
            "from": "Indo-European",  
            "to": "English",  
            "label": "属于",  
            "color": "grey"  
        }  
    ],  
    "clusters": [  
        {  
            "id": "cluster_china",  
            "label": "中国语言",  
            "color": "red",  
            "nodes": ["Mandarin", "Cantonese", "Wu"],  
            "position": "bottom"  
        },  
        // ... 其他集群  
    ]  
}
```
