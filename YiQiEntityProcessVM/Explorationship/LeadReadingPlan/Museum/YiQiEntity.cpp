#include <iostream>
#include <string>
#include <memory>
#include <vector>

// YiQiEntity 类
class YiQiEntity {
public:
    std::string name;
    YiQiEntity(std::string name) : name(name) {}
    virtual void print() const {
        std::cout << "YiQiEntity Name: " << name << std::endl;
    }
};

// AtomComposition 类
class AtomComposition {
public:
    std::string type;
    AtomComposition(std::string type) : type(type) {}
};

// YiQiEntityVM 类
class YiQiEntityVM {
public:
    std::shared_ptr<YiQiEntity> entity;
    LeadReadingPlan plan;

    YiQiEntityVM(std::shared_ptr<YiQiEntity> e, LeadReadingPlan p)
        : entity(std::move(e)), plan(p) {}

    void printInfo() const {
        std::cout << "Lead Reading Plan: ";
        switch (plan) {
            case LeadReadingPlan::TransportshipLeadReadingPlan:
                std::cout << "Transportship";
                break;
            case LeadReadingPlan::ArsenalshipLeadReadingPlan:
                std::cout << "Arsenalship";
                break;
            case LeadReadingPlan::ExplorationshipLeadReadingPlan:
                std::cout << "Explorationship";
                break;
        }
        std::cout << ", YiQiEntity Name: ";
        if (entity) {
            entity->print();
        } else {
            std::cout << "None";
        }
        std::cout << std::endl;
    }

    void storeData(const std::string& data) {
        std::cout << "Storing data: " << data << " for VM with plan: ";
        switch (plan) {
            case LeadReadingPlan::TransportshipLeadReadingPlan:
                std::cout << "Transportship";
                break;
            case LeadReadingPlan::ArsenalshipLeadReadingPlan:
                std::cout << "Arsenalship";
                break;
            case LeadReadingPlan::ExplorationshipLeadReadingPlan:
                std::cout << "Explorationship";
                break;
        }
        std::cout << std::endl;
    }

    void communicate(const std::string& message) {
        std::cout << "Communicating message: " << message << " from VM with plan: ";
        switch (plan) {
            case LeadReadingPlan::TransportshipLeadReadingPlan:
                std::cout << "Transportship";
                break;
            case LeadReadingPlan::ArsenalshipLeadReadingPlan:
                std::cout << "Arsenalship";
                break;
            case LeadReadingPlan::ExplorationshipLeadReadingPlan:
                std::cout << "Explorationship";
                break;
        }
        std::cout << std::endl;
    }

    void compute() const {
        std::cout << "Computing for VM with plan: ";
        switch (plan) {
            case LeadReadingPlan::TransportshipLeadReadingPlan:
                std::cout << "Transportship";
                break;
            case LeadReadingPlan::ArsenalshipLeadReadingPlan:
                std::cout << "Arsenalship";
                break;
            case LeadReadingPlan::ExplorationshipLeadReadingPlan:
                std::cout << "Explorationship";
                break;
        }
        std::cout << std::endl;
    }
};

// YiQiEntityVMAtomComposition 类
class YiQiEntityVMAtomComposition {
public:
    AtomComposition composition;
    YiQiEntityVM vm;

    YiQiEntityVMAtomComposition(std::string atomType, std::shared_ptr<YiQiEntity> entity, LeadReadingPlan plan)
        : composition(atomType), vm(std::move(entity), plan) {}

    void printVMInfo() const {
        vm.printInfo();
    }

    void storeData(const std::string& data) {
        std::cout << "Storing data: " << data << " for AtomComposition of type: " << composition.type << std::endl;
        vm.storeData(data);
    }

    void communicate(const std::string& message) {
        std::cout << "Communicating message: " << message << " for AtomComposition of type: " << composition.type << std::endl;
        vm.communicate(message);
    }

    void compute() const {
        std::cout << "Computing for AtomComposition of type: " << composition.type << std::endl;
        vm.compute();
    }
};

// YiQiEntityVMManager 类
class YiQiEntityVMManager {
public:
    std::vector<std::shared_ptr<YiQiEntityVMAtomComposition>> compositions;

    void addComposition(std::shared_ptr<YiQiEntityVMAtomComposition> comp) {
        compositions.push_back(std::move(comp));
    }

    void removeComposition(size_t index) {
        if (index < compositions.size()) {
            compositions.erase(compositions.begin() + index);
        }
    }

    std::shared_ptr<YiQiEntityVMAtomComposition> getComposition(size_t index) const {
        if (index < compositions.size()) {
            return compositions[index];
        }
        return nullptr;
    }

    void globalCompute() const {
        for (const auto& comp : compositions) {
            comp->compute();
        }
    }

    void globalStoreData(const std::string& data) {
        for (const auto& comp : compositions) {
            comp->storeData(data);
        }
    }

    void globalCommunicate(const std::string& message) {
        for (const auto& comp : compositions) {
            comp->communicate(message);
        }
    }
};

// 示例用法
int main() {
    // 创建 YiQiEntity 实例
    auto entity = std::make_shared<YiQiEntity>("Space Explorer");

    // 创建 YiQiEntityVMAtomComposition 实例
    auto composition = std::make_shared<YiQiEntityVMAtomComposition>("Carbon", entity, LeadReadingPlan::ExplorationshipLeadReadingPlan);

    // 创建 YiQiEntityVMManager 实例
    YiQiEntityVMManager manager;
    manager.addComposition(composition);

    // 打印信息
    composition->printVMInfo();

    // 存储数据
    manager.globalStoreData("Some data to store");

    // 通信
    manager.globalCommunicate("Hello, world!");

    // 计算
    manager.globalCompute();

    return 0;
}