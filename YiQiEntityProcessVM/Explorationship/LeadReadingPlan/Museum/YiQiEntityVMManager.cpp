#include <iostream>
#include <string>
#include <memory>
#include <vector>
#include <unordered_map>

// Message 类
class Message {
public:
    std::string content;
    size_t recipientId; // 接收者的标识

    Message(std::string content, size_t recipientId)
        : content(std::move(content)), recipientId(recipientId) {}
};

// MessageQueue 类
class MessageQueue {
public:
    std::vector<Message> messages;

    void enqueue(const Message& msg) {
        messages.push_back(msg);
    }

    bool isEmpty() const {
        return messages.empty();
    }

    Message dequeue() {
        if (!messages.empty()) {
            Message front = messages.front();
            messages.erase(messages.begin());
            return front;
        }
        throw std::runtime_error("Queue is empty.");
    }
};

// MessageSubscriber 类
class MessageSubscriber {
public:
    size_t id;
    MessageQueue queue;

    MessageSubscriber(size_t id) : id(id) {}

    void receiveMessage(const Message& msg) {
        if (msg.recipientId == id) {
            queue.enqueue(msg);
        }
    }

    void processMessages() {
        while (!queue.isEmpty()) {
            try {
                Message msg = queue.dequeue();
                std::cout << "Processing message: " << msg.content << " for subscriber: " << id << std::endl;
            } catch (const std::exception& e) {
                std::cerr << "Error: " << e.what() << std::endl;
            }
        }
    }
};

// YiQiEntityVMAtomComposition 类
class YiQiEntityVMAtomComposition : public MessageSubscriber {
public:
    AtomComposition composition;
    YiQiEntityVM vm;

    YiQiEntityVMAtomComposition(size_t id, std::string atomType, std::shared_ptr<YiQiEntity> entity, LeadReadingPlan plan)
        : MessageSubscriber(id), composition(atomType), vm(std::move(entity), plan) {}

    void printVMInfo() const {
        vm.printInfo();
    }

    void storeData(const std::string& data) {
        std::cout << "Storing data: " << data << " for AtomComposition of type: " << composition.type << std::endl;
        vm.storeData(data);
    }

    void communicate(const std::string& message) {
        std::cout << "Communicating message: " << message << " for AtomComposition of type: " << composition.type << std::endl;
        vm.communicate(message);
    }

    void compute() const {
        std::cout << "Computing for AtomComposition of type: " << composition.type << std::endl;
        vm.compute();
    }

    void receiveMessage(const Message& msg) override {
        MessageSubscriber::receiveMessage(msg);
    }
};

// YiQiEntityVMManager 类
class YiQiEntityVMManager {
public:
    std::unordered_map<size_t, std::shared_ptr<YiQiEntityVMAtomComposition>> subscribers;
    MessageQueue globalQueue;

    void addSubscriber(std::shared_ptr<YiQiEntityVMAtomComposition> comp) {
        subscribers[comp->id] = std::move(comp);
    }

    void removeSubscriber(size_t id) {
        subscribers.erase(id);
    }

    std::shared_ptr<YiQiEntityVMAtomComposition> getSubscriber(size_t id) const {
        auto it = subscribers.find(id);
        if (it != subscribers.end()) {
            return it->second;
        }
        return nullptr;
    }

    void sendMessage(const Message& msg) {
        globalQueue.enqueue(msg);
    }

    void processGlobalMessages() {
        while (!globalQueue.isEmpty()) {
            try {
                Message msg = globalQueue.dequeue();
                auto subscriber = getSubscriber(msg.recipientId);
                if (subscriber) {
                    subscriber->receiveMessage(msg);
                } else {
                    std::cerr << "Recipient not found for message: " << msg.content << std::endl;
                }
            } catch (const std::exception& e) {
                std::cerr << "Error processing global messages: " << e.what() << std::endl;
            }
        }
    }

    void globalCompute() const {
        for (const auto& [id, comp] : subscribers) {
            comp->compute();
        }
    }

    void globalStoreData(const std::string& data) {
        for (const auto& [id, comp] : subscribers) {
            comp->storeData(data);
        }
    }

    void globalCommunicate(const std::string& message) {
        for (const auto& [id, comp] : subscribers) {
            comp->communicate(message);
        }
    }
};

// 示例用法
int main() {
    // 创建 YiQiEntity 实例
    auto entity = std::make_shared<YiQiEntity>("Space Explorer");

    // 创建 YiQiEntityVMAtomComposition 实例
    auto composition1 = std::make_shared<YiQiEntityVMAtomComposition>(1, "Carbon", entity, LeadReadingPlan::ExplorationshipLeadReadingPlan);
    auto composition2 = std::make_shared<YiQiEntityVMAtomComposition>(2, "Silicon", entity, LeadReadingPlan::ArsenalshipLeadReadingPlan);

    // 创建 YiQiEntityVMManager 实例
    YiQiEntityVMManager manager;
    manager.addSubscriber(composition1);
    manager.addSubscriber(composition2);

    // 发送消息
    manager.sendMessage(Message("Hello, world!", 1));
    manager.sendMessage(Message("Goodbye, world!", 2));

    // 处理消息
    manager.processGlobalMessages();

    // 打印信息
    composition1->printVMInfo();
    composition2->printVMInfo();

    // 存储数据
    manager.globalStoreData("Some data to store");

    // 通信
    manager.globalCommunicate("Hello, world!");

    // 计算
    manager.globalCompute();

    return 0;
}