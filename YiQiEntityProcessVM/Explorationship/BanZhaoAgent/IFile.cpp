#include <iostream>  
#include <memory>  
#include <vector>  
#include <string>  
  
class IFile {  
public:  
    virtual ~IFile() {}  
    virtual void open(const std::string& path) = 0;  
    virtual std::string read(size_t size) = 0;  
    virtual void write(const std::string& data) = 0;  
    virtual void close() = 0;  
};  
  
class MemoryFile : public IFile {  
private:  
    std::string data;  
  
public:  
    void open(const std::string& path) override {  
        // 简化实现，忽略路径  
        data = "Initial data";  
    }  
  
    std::string read(size_t size) override {  
        std::string result = data.substr(0, size);  
        data = data.substr(size);  // 模拟数据被读取后移除  
        return result;  
    }  
  
    void write(const std::string& data) override {  
        this->data += data;  
    }  
  
    void close() override {}  
};  
  
// 假设还有其他类型的文件，如ProcessFile, NetworkFile等

#include <string>  
#include <iostream>  
  
class ProcessFile : public IFile {  
private:  
    std::string processData; // 模拟存储的进程数据  
  
public:  
    ProcessFile() : processData("Initial process data") {}  
  
    void open(const std::string& path) override {  
        // 在这个模拟中，我们忽略路径，并假设总是打开相同的“进程”  
        std::cout << "Opening process file..." << std::endl;  
    }  
  
    std::string read(size_t size) override {  
        std::string result = processData.substr(0, size);  
        processData = processData.substr(size); // 模拟数据被读取后移除  
        return result;  
    }  
  
    void write(const std::string& data) override {  
        // 在这个模拟中，我们不支持向进程文件写入，但可以抛出异常或记录日志  
        throw std::runtime_error("Cannot write to a process file.");  
    }  
  
    void close() override {  
        std::cout << "Closing process file..." << std::endl;  
    }  
};
#include <string>  
#include <iostream>  
  
class NetworkFile : public IFile {  
private:  
    std::string networkData; // 模拟从网络接收的数据  
  
public:  
    NetworkFile() : networkData("Initial network data") {}  
  
    void open(const std::string& path) override {  
        // 在这个模拟中，路径可能代表网络地址或连接标识符  
        // 但我们在这里仅使用它来打印信息  
        std::cout << "Connecting to network file at: " << path << std::endl;  
    }  
  
    std::string read(size_t size) override {  
        std::string result = networkData.substr(0, size);  
        networkData = networkData.substr(size); // 模拟数据被读取后移除  
        return result;  
    }  
  
    void write(const std::string& data) override {  
        // 在这个模拟中，我们简单地将数据追加到networkData，但实际上应该发送到网络  
        networkData += data;  
        std::cout << "Writing to network: " << data << std::endl;  
    }  
  
    void close() override {  
        std::cout << "Closing network connection..." << std::endl;  
    }  
};
class Plugin {  
public:  
    virtual ~Plugin() {}  
    virtual void execute(std::shared_ptr<IFile> file) = 0;  
};  
  
class SimplePlugin : public Plugin {  
public:  
    void execute(std::shared_ptr<IFile> file) override {  
        file->open("/dummy");  
        std::string content = file->read(10);  
        std::cout << "Read from file: " << content << std::endl;  
        file->write("New data");  
        file->close();  
    }  
};  
  
// 插件管理器，用于加载和执行插件  
class PluginManager {  
private:  
    std::vector<std::unique_ptr<Plugin>> plugins;  
  
public:  
    void addPlugin(std::unique_ptr<Plugin> plugin) {  
        plugins.push_back(std::move(plugin));  
    }  
  
    void runPlugins(std::shared_ptr<IFile> file) {  
        for (auto& plugin : plugins) {  
            plugin->execute(file);  
        }  
    }  
};

int main() {  
    auto file = std::make_shared<MemoryFile>();  
    PluginManager manager;  
    manager.addPlugin(std::make_unique<SimplePlugin>());  
    manager.runPlugins(file);  
  
    return 0;  
}