#include <string>

// 假设这是游戏引擎提供的类型
typedef const char* pcstr;

// 速度参数结构体
struct SVelocityParam {
    // 成员变量和方法
};

// 动画管理器接口
class IAnimationManager {
public:
    void AddAnim(int animId, const char* animationName, int priority, SVelocityParam* velocity, int state);
};

// 控制管理器接口
class IControlManager {
public:
    void add(void* ability, int type);
};

// 攻击移动参数结构体
struct attack_on_move_params_t {
    bool enabled;
    float far_radius;
    float attack_radius;
    float update_side_period;
    float prediction_factor;
    float prepare_time;
    float prepare_radius;
    float max_go_close_time;
};

// 反瞄准能力类
class anti_aim_ability {
public:
    anti_aim_ability(CBaseShip* owner);
    void load_from_ini(const char* pSettings, const char* section);
    // 其他方法
private:
    CBaseShip* m_owner;
    // 成员变量
};

// 基础舰船类（敌我友识别省略；）
class CBaseShip {
public:
    CBaseShip();
    ~CBaseShip();

    void PostLoad(const char* section);

    attack_on_move_params_t& getAttackOnMoveParams() { return m_attack_on_move_params; }
    IAnimationManager& anim() { return *m_animationManager; }
    IControlManager& control() { return *m_controlManager; }
    SVelocityParam& move_getVelocity(int velocityType);

private:
    attack_on_move_params_t m_attack_on_move_params;
    anti_aim_ability* m_anti_aim = nullptr;
    IAnimationManager* m_animationManager;
    IControlManager* m_controlManager;
};

CBaseShip::CBaseShip()
    : m_animationManager(&g_AnimationManager),  // 假设全局动画管理器已经定义
      m_controlManager(&g_ControlManager)      // 假设全局控制管理器已经定义
{}

CBaseShip::~CBaseShip()
{
    delete m_anti_aim;
}

void CBaseShip::PostLoad(const char* section)
{
    attack_on_move_params_t& aom = m_attack_on_move_params;

    aom.enabled = (READ_IF_EXISTS(pSettings, r_bool, section, "aom_enabled", FALSE) != 0);
    aom.far_radius = READ_IF_EXISTS(pSettings, r_float, section, "aom_far_radius", detail::base_Ship::aom_far_radius);
    aom.attack_radius = READ_IF_EXISTS(pSettings, r_float, section, "aom_attack_radius", detail::base_Ship::aom_attack_radius);
    aom.update_side_period = READ_IF_EXISTS(pSettings, r_float, section, "aom_update_side_period", detail::base_Ship::aom_update_side_period);
    aom.prediction_factor = READ_IF_EXISTS(pSettings, r_float, section, "aom_prediction_factor", detail::base_Ship::aom_prediction_factor);
    aom.prepare_time = READ_IF_EXISTS(pSettings, r_float, section, "aom_prepare_time", detail::base_Ship::aom_prepare_time);
    aom.prepare_radius = READ_IF_EXISTS(pSettings, r_float, section, "aom_prepare_radius", detail::base_Ship::aom_prepare_radius);
    aom.max_go_close_time = READ_IF_EXISTS(pSettings, r_float, section, "aom_max_go_close_time", 8.f);

    if (aom.enabled) {
        SVelocityParam& velocity_run = move_getVelocity(ShipMovement::eVelocityParameterRunNormal);

        pcstr attack_on_move_anim_l = READ_IF_EXISTS(pSettings, r_string, section, "aom_animation_left", "stand_attack_run_");
        anim().AddAnim(eAnimAttackOnRunLeft, attack_on_move_anim_l, -1, &velocity_run, PS_STAND);
        pcstr attack_on_move_anim_r = READ_IF_EXISTS(pSettings, r_string, section, "aom_animation_right", "stand_attack_run_");
        anim().AddAnim(eAnimAttackOnRunRight, attack_on_move_anim_r, -1, &velocity_run, PS_STAND);
    }

    if (pSettings->line_exist(section, "anti_aim_effectors")) {
        SVelocityParam& velocity_stand = move_getVelocity(ShipMovement::eVelocityParameterStand);

        m_anti_aim = new anti_aim_ability(this);
        control().add(m_anti_aim, ControlCom::eAntiAim);

        pcstr anti_aim_animation = READ_IF_EXISTS(pSettings, r_string, section, "anti_aim_animation", "stand_attack_");
        anim().AddAnim(eAnimAntiAimAbility, anti_aim_animation, -1, &velocity_stand, PS_STAND);
        m_anti_aim->load_from_ini(pSettings, section);
    }
}